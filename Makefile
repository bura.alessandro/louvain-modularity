all: louvain

louvain: louvain.o graph.o community.o linkedlist.o
	gcc -o louvain louvain.o graph.o community.o linkedlist.o -lm

louvain.o: src/louvain.c src/graph.h src/community.h
	gcc -c src/louvain.c -O2

graph.o: src/graph.c src/graph.h
	gcc -c src/graph.c -O2
	
community.o: src/community.c src/graph.h src/community.h linkedlist.o
	gcc -c src/community.c -O2
	
linkedlist.o: src/linkedlist.c src/linkedlist.h
	gcc -c src/linkedlist.c -O2
	
clean:
	rm -f *.o louvain
