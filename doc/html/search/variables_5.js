var searchData=
[
  ['next',['next',['../structgnode__t.html#acad7921625f7ae519d99ba48df283431',1,'gnode_t::next()'],['../structnode__t.html#aca548c9bafe54ba9db19d40a7a7f1cb3',1,'node_t::next()']]],
  ['nodes',['nodes',['../structcommunity__t.html#ae0af7db305b8b53967d8b9bdf70fa6c1',1,'community_t']]],
  ['num_5fcommunities',['num_communities',['../structcommunity__set__t.html#ace8677db03be7e2a18dcb539cadd2366',1,'community_set_t']]],
  ['num_5fedges',['num_edges',['../structgraph__t.html#a9044c89b78d8d87e2e4cbc10edf726ef',1,'graph_t']]],
  ['num_5fnodes',['num_nodes',['../structcommunity__t.html#ac0fef4f0ce4389e2d3578e85588a83d8',1,'community_t']]],
  ['num_5fvertices',['num_vertices',['../structgraph__t.html#a74014409984ec0afc2835c6ac1bd06c7',1,'graph_t']]]
];
