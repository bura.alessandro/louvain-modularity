# Louvain modularity

C implementation of Louvain Modularity Algorithm

1 Algorithm
============
The Louvain algorithm for community detection is a modularity optimization algorithm that is divided into two main phases:

1.1 Phase 1
-----------
In the first phase the algorithm creates a community for each node of the graph and the moves nodes into their neighboring community in order to maximize modularity locally.

1.2 Phase 2
-----------
In the second phase the algorithm creates a new graph starting from the optimized communities: a self loop symbolizes the sum of the links inside the community, whereas a link to another node indicates the sum of all the weights of the links connecting the two communities.
Once the second phase has ended the algorithm starts again if there is still a potential modularity increase.

2 Implementation
=================
The algorithm is implemented to support input of unweighted (the weight of a link is automatically supposed to be 1) and undirected graphs.

3 Usage
========
To compile the program simply launch `make` from terminal and to clean all of the object files and executable launch `make clean`.
The program doesn't do much arguments checking, so it relies on the friendly input by the user. The correct usage is

`./louvain <input_filename> [-p <number_of_passes>]`

where number_of passes can be any integer value (if the user does not specify the number of passes it is automatically assumed to be -1). Usually a good value is between 1 and 5. Keep in mind that the output of the program changes depending on the number of passes done.

It is highly recommended to **redirect the output to a file** in order to be able to analize it later on, especially if the input is a very big network.

The program correctly handles networks with 100k nodes. If the user finds this bound restrictive, he can modify MAX_NODES in graph.h keeping in mind that the limit is the maximum value contained by an integer. There is also an upper bound to the maximum number of links (aka lines of the input file) defined in macro MAX_LINES in louvain.c.
