/*
 ============================================================================
 Name        : community.h
 Author      : Buratto Alessandro
 ============================================================================
 */
#include "graph.h"
#include "linkedlist.h"

#ifndef COMMUNITY_H_
#define COMMUNITY_H_

/**
 * @struct community_set_t
 * Structure to contain a set of communities along with the pointer to the graph that contains the nodes in those communities.
 * It also contains the total number of communities for easy access.
 * @struct community_t
 * Structure to contain a single community with the list of all its nodes alongside with the internal and external weight of all of its edges (INTERNAL = edges fully contained in the community; EXTERNAL = edges with one end in the community and the other end somewhere else) and the total number of nodes for easy access.
 */
typedef struct community_t {int internal_weight, external_weight, num_nodes; node_t * nodes;} community_t;
typedef struct community_set_t {community_t * c; graph_t * g; int num_communities;} community_set_t;

community_set_t * create_community_set(graph_t * graph);

void add_node_to_community(community_set_t * set, int comm_index, int node_id);

void remove_node_from_community(community_set_t * set, int comm_index, int node_id);

char * neighboring_commuities(community_set_t * set, int comm_index);

int find_community(community_set_t * set, int target);

int possible_new_links(community_set_t * set, int comm_index, int node_id);

double modularity(community_set_t * set);

void collapse(community_set_t * set);

void clean_set(community_set_t * set);

void print_communities(community_set_t * set);

#endif /* COMMUNITY_H_ */
