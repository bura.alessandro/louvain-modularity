/*
 ============================================================================
 Name        : graph.c
 Author      : Buratto Alessandro
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "graph.h"

//=============================================================
// "Private" functions to be used in the following methods

/**
 * Utility function to add a new node to the head of the list.
 * @param head the list head address.
 * @param val The value of the node.
 * @param weight The weight of the link.
 */
void add_head(gnode_t ** head, int val, int weight) {
	gnode_t * new_node = (gnode_t *) malloc(sizeof(gnode_t));
	new_node->dest = val;
	new_node->weight = weight;
	new_node->next = *head;
	*head = new_node;
}

/**
 * Utility function to remove an entry from an adjacency list.
 * @param list_head The head of the adjacency list.
 * @param target The entry id to remove.
 * @return 0 if the removal didn't end well, 1 if the function finished correctly
 */
int remove_entry(gnode_t ** list_head, int target) {
	//removes the edge entry from source list
	int it = 0; //iteration counter to discover if the removed edge is the only one
	gnode_t * curr = *list_head;
	gnode_t * prev = curr;

	while (curr->dest != target && curr) {
		prev = curr;
		curr = curr->next;
		++it;
	}
	//Case 1: the edge doesn't exist
	if (curr == NULL) {
		puts("Il lato da eliminare non esiste");
		return 0;
	}
	//Case 2: the edge exists and is in a place different than 0
	if (it) {
		prev->next = curr->next;
		free(curr);
	}
	//Case 3: the edge exists but it's the first in the list
	else if (it == 0) {
		gnode_t * next_node = NULL;
		next_node = curr->next;
		free(curr);
		curr = next_node;
	}
	return 1;
}

//==============================================================

/**
 * A function to create a graph starting from the edges in format source - destination.
 * @param edges The array of edges of the graph you want to create.
 * @param n The number of edges contained in the array.
 * @return A pointer to the graph.
 */
graph_t * create_graph(edge_t * edges, int n) {

	//allocates memory for the graph
	graph_t * graph = (graph_t *) calloc(1, sizeof(graph_t));

	/*allocates enough memory to contain a number of vertices
	 * equal to the number of edges (it will be reduced later)*/
	graph->vertices = (gnode_t **) calloc(MAX_NODES, sizeof(gnode_t *));

	//fills the graph
	for(int i = 0; i < n; ++i) {
		add_edge(graph, edges[i]);
	}

	//frees unnecessary allocated memory for the vertices
	gnode_t ** new_v = (gnode_t **) calloc(graph->num_vertices, sizeof(gnode_t *));
	memcpy(new_v, graph->vertices, sizeof(gnode_t *) * graph->num_vertices);
	free(graph->vertices);
	graph->vertices = new_v;

	return graph;
}

/**
 * Function to return the number of edges in the graph.
 * @param g The graph one wants to know the number of edges.
 * @return The number of edges.
 */
int num_edges(graph_t * g) {
	return g->num_edges;
}

/**
 * Function to return the number of vertices in the graph.
 * @param g The graph one wants to know the number of vertices.
 * @return The number of vertices.
 */
int num_vertices(graph_t * g) {
	return g->num_vertices;
}

/**
 *  Function to calculate the weighted degree of a node.
 *
 * @param g The pointer to the graph which contains the node.
 * @param node_id The index of the node.
 * @return The weighted degree of the given node.
 */
int degree(graph_t * g, int node_id) {
	gnode_t * head = g->vertices[node_id];
	int out = 0;
	while(head) {
		out += head->weight;
		head = head->next;
	}
	return out;
}

/**
 * Function to insert a new edge in a given graph.
 * It has to add the edge both in the source vertex and in the destination edge's list.
 * @param g The graph one wants to modify.
 * @param e The edge that one wants to add.
 */
void add_edge(graph_t * g, edge_t e) {
	gnode_t ** vertices = g->vertices;
	int src = e.src;
	int dest = e.dest;

	//indices out of bounds
	if(src > MAX_NODES || dest > MAX_NODES) {
		puts("Il nodo da inserire e' piu' grande della capacita' massima del grafo");
		exit(1);
	}

	//not a self loop
	if (src != dest) {
		//updates the number of vertices in the graph
		if(vertices[src] == NULL) {
		++(g->num_vertices);
		}

		if(vertices[dest] == NULL) {
			++(g->num_vertices);
		}

		add_head(&vertices[src], dest, e.weight); //push in the src position
		add_head(&vertices[dest], src, e.weight); //push in the dest position
	}

	//case of a self loop
	else {
		if(vertices[src] == NULL) {
			++(g->num_vertices);
		}
		add_head(&vertices[src], dest, e.weight); //push in the src position
	}
	//adds 1 to the edges count of the graph
	g->num_edges += 1;
	g->tot_link_weight += e.weight; //adds the weight of the new link to the total
}

/**
 * Function to remove an edge from a given graph.
 * It removes the edge from both the vertices' lists that were involved in the connection.
 * @param g The origin graph.
 * @param e The edge one wants to remove.
 */
void remove_edge(graph_t * g, edge_t e) {
	int src = e.src;
	int dest = e.dest;

	//case of a self loop
	if(src == dest) {
		(void) remove_entry(&(g->vertices[src]), src);
	}
	else {
		int a = remove_entry(&(g->vertices[src]), dest);
		int b = remove_entry(&(g->vertices[dest]), src);

		if (!a || !b) {
			puts("Non e' stato possibile eliminare il lato");
			return;
		}
	}

	g -> num_edges = g -> num_edges - 1;
	g->tot_link_weight -= e.weight;//removes the weight of the edge from the total
}

/**
 * Function to free all allocated memory for the graph.
 * It iteratively frees memory for all edges, then for all vertices and then for the graph itself.
 * @param g The graph one wants to remove.
 */
void clean_graph(graph_t * g) {
	gnode_t ** vertices = g->vertices;
	int n = g->num_vertices;
	for (int i = n - 1; i >= 0; --i) {
		gnode_t * list = vertices[i];
		if (list == NULL)
			free(list);
		else {
			gnode_t * curr = list->next;
			while(curr) {
				gnode_t * temp = curr->next;
				free(curr);
				curr = temp;
			}
			free(list);
		}
	}
	free(vertices);
	free(g);
}

/**
 * Function to print to stdout the adjacency list of every node in the graph.
 * @param g the graph one wants to print
 */
void print_graph(graph_t * g) {
	int n = g->num_vertices;
	for(int i = 0; i < n; ++i) {
		gnode_t * curr = g->vertices[i];
		printf("Archi del nodo %d: ", i);
		while(curr) {
			printf("dest:%d w:%d | ", curr->dest, curr->weight);
			curr = curr->next;
		}
		//if the vertex of the graph is a null pointer then it has no edges
		if(g->vertices[i] == NULL)
			printf("VUOTA");
		puts("");
	}
	puts("");
}

