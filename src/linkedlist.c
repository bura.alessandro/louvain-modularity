/*
 ============================================================================
 Name        : linkedlist.c
 Author      : Buratto Alessandro
 ============================================================================
 */

#include <stdlib.h>
#include <stdio.h>

#include "linkedlist.h"
/**
 * Function to remove the head node also freeing its allocated memory.
 * @param head A pointer to the pointer of the first node in the list
 * @return -1 if the list was already empty, THE VALUE CONTAINED IN THE HEAD NODE otherwise.
 */
int pop(node_t ** head) {
	int retval = -1;
	if (*head == NULL) {
		return retval;
	}
	node_t * next_node = NULL;
	next_node = (*head)->next;
	retval = (*head)->val;
	free(*head);
	*head = next_node;
	return retval;
}

/**
 * Function to put a new head containing the specified value to a given list.
 * @param head A pointer to the pointer containing the address of the head of the list.
 * @param val The value to be assigned to the new head.
 */
void push(node_t ** head, int val) {
	node_t * new_node = NULL;
	new_node = (node_t *) malloc(sizeof(node_t));
	new_node->val = val;
	new_node->next = *head;
	*head = new_node;
}

/**
 * Function to remove a node from the list giving the value contained in it.
 * @param head A pointer to the pointer containing the address of the head of the list.
 * @param number The value contained in the node to be removed.
 * @return -1 if the removal failed, 0 if the node was in the head, 1 if it was anywhere else.
 */
int remove_index(node_t ** head, int number) {

	node_t * current = *head;
	node_t * prev = NULL;
	int it = 0;//iteration count

	//runs through the list until there's a match
	while (current) {
		//the out node is the head
		if (it == 0 && number == current->val) {
				(void) pop(head);
				return 0;//node in head
			}
		//node found in the list and it's not the head
		if(current->val == number) {
			prev->next = current->next;
			free(current);
			return 1; //node in the middle
		}
		//next in the list
		prev = current;
		current = current->next;
		++it;
	}
	return -1;//failed removal
}

/**
 * Function to clear the memory allocated for the list.
 * @param head The pointer to the list head.
 */
void clear(node_t * head) {
	while(head) {
		node_t * temp = head;
		head = head->next;
		free(temp);
	}
}
