/*
 ============================================================================
 Name        : community.c
 Author      : Buratto Alessandro
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "community.h"
#include "graph.h"
#include "linkedlist.h"

/*===========================================================================
 * Utility functions
 */
/**
 * Utility function to calculate the internal and external weights of a community.
 * @param comm Pointer to the community of interest.
 * @param graph	Pointer to the graph containing the nodes of the community.
 */
void calculate_weights(community_t * comm, graph_t * graph) {

	node_t * list = comm->nodes;
	comm->internal_weight = 0;
	comm->external_weight = 0;

	//array to store all the nodes in the community
	char * nodes = (char *) calloc(graph->num_vertices, sizeof(char));
	while(list) {
		nodes[list->val] = 1;
		list = list->next;
	}

	//for each node in the community calculate the in and ext edge weight
	for(int i = 0; i < graph->num_vertices; ++i) {
		if(nodes[i]) {
			gnode_t * edge = graph->vertices[i];
			while(edge) {
				//node not already counted
				if(edge->dest >= i) {
					//self loop or inner edge
					if(nodes[edge->dest]) {
						comm->internal_weight += edge->weight;
					}
					else {
						comm->external_weight += edge->weight;
					}
				}
				else if(nodes[edge->dest]){}//already counted
				else//it's an external edge
					comm->external_weight += edge->weight;
				edge = edge->next;
			}
		}
	}

	free(nodes);
}

/* End of utility functions
 * ==========================================================================
 */
/**
 * Function to create a set of communities each containing a single node of the given graph.
 * @param graph The graph from which one wants to create the set of communities (one for each node).
 * @return A pointer to a new filled up set of communities.
 */

community_set_t * create_community_set(graph_t * graph) {
	//dynamically allocates memory for the community set
	community_set_t * set = (community_set_t *) malloc(sizeof(community_set_t));
	set -> c = (community_t *) calloc(graph->num_vertices, sizeof(community_t));
	set -> g = graph;
	set->num_communities = graph->num_vertices;

	for(int i = 0; i < set->num_communities; ++i) {
		community_t * comm = &(set->c[i]);
		comm->internal_weight = 0;
		comm->nodes = (node_t *) calloc(1, sizeof(node_t));
		comm->nodes->val = i;

		//calculates the external weight of the community connections
		gnode_t * curr = graph->vertices[i]; //current considered node
		while(curr){
			//if self loop update internal weight instead
			if (i == curr->dest) {
				comm->internal_weight += curr->weight;
			}
			else {
				comm->external_weight += curr->weight;
			}
			curr = curr->next;
		}

		comm->num_nodes = 1;//all communities have only one node
	}

	return set;
}

/**
 * Function to add a node into a given community.
 * It makes sure to update also the total inner and external weight count (as this improves speed in modularity calculation).
 * @param set The set of communities.
 * @param comm_index The index of the community to be modified in the set.
 * @param node_id The id of the node to be added.
 */
void add_node_to_community(community_set_t * set, int comm_index, int node_id) {
	//detects the community to be updated
	graph_t * graph = set->g;
	community_t * comm = &(set->c[comm_index]);

	push(&(comm->nodes), node_id); //adds node to node list

	//adjourns the values in the community
	calculate_weights(comm, graph);

	comm->num_nodes += 1;//updates the number of nodes in the community
}

/**
 * Function to remove a node into a given community.
 * It makes sure to update also the total inner and external weight count (as this improves modularity calculation).
 * @param set The set of communities.
 * @param comm_index The index of the community to be modified in the set.
 * @param node_id The id of the node to be added.
 */
void remove_node_from_community(community_set_t * set, int comm_index, int node_id) {
	community_t * comm = &(set->c[comm_index]);
	int result = remove_index(&(comm->nodes), node_id);
	//recalculate internal and external weights
		calculate_weights(comm, set->g);
	//managed to take the node out of the community
	if(result != -1) {
		comm->num_nodes -= 1;
	}
	//failed removal
	else {
	puts("Errore nell'eliminazione del nodo dalla comunita'");
	}
}

/**
 * Function to produce an array containing '1' if the community is a neighbor to the specified one (or it's the same community) or '0' if the community isn't a neighbor of the one indexed.
 * A community is said to be a neighbor of another one if exists at least a link between two of their nodes.
 * @param set The set of communities.
 * @param node_id Index of the node one wants to find the neighbor communities of.
 * @return Dynamically allocated array of "boolean" values to identify neighboring communities. No need to specify the length for it has a number of elements equal to the num_communities of the set.
 */
char * neighboring_commuities(community_set_t * set, int node_id) {
	char * out = (char *) calloc(set->num_communities, sizeof(char));
	graph_t * g = set->g;
	gnode_t * links = g->vertices[node_id];

	while(links) {
		out[find_community(set, links->dest)] = 1;
		links = links->next;
	}

	return out;
}

/**
 * Function that returns the index of the community that contains the specified node
 * @param set The set of communities
 * @param target The index of the node one wants to find the community
 * @return The index of the community that contains the targeted node. -1 if not found
 */
int find_community(community_set_t * set, int target) {
	int def = -1;
	community_t * comm = set->c;
	for(int i = 0; i < set->num_communities;++i) {
		//search in each community node lists
		node_t * head = comm[i].nodes;
		while(head) {
			if(head->val == target)
				return i; //return the index of the community
			head = head->next;
		}
	}
	return def;
}

/**
 * Function to calculate the possible new links increase to the internal weight of the community if the given node was added.
 * @param set The set of communities.
 * @param comm_index The index of the community.
 * @param node_id The number of the given node.
 * @return The sum of all the possible new links's weight.
 */
int possible_new_links(community_set_t * set, int comm_index, int node_id) {
	int out = 0;
	graph_t * graph = set->g;
	community_t * curr = &(set->c[comm_index]);
	node_t * list = curr->nodes;//list of nodes in the community
	//array to store all the nodes in the community
		char * nodes = (char *) calloc(graph->num_vertices, sizeof(char));
		while(list) {
			nodes[list->val] = 1;
			list = list->next;
		}

	gnode_t * links = graph->vertices[node_id];
	while(links) {
		if(nodes[links->dest]) {
			out += links->weight;
		}
		links = links->next;
	}

	free(nodes);//frees allocated memory

	return out;
}

/**
 * Function to calculate the modularity of a set of communities.
 * @param set The given set of communities.
 * @return The modularity of the set.
 */
double modularity(community_set_t * set) {
	double out = 0.;
	double m2 = (double) (set->g->tot_link_weight * 2);//double the total weight of all links in graph
	community_t * curr = set->c;
	for(int i = 0; i < set->num_communities; ++i) {
		out += ((double)curr->internal_weight)/m2 - pow(((double) curr->external_weight+curr->internal_weight) / m2, 2);
		curr++;
	}

	return out;
}

/**
 * Function to reduce the dimension of a community set after a pass of the Louvain algorithm.
 * It removes the communities that have no nodes in them.
 * @param set A pointer to a set of communities.
 */
void collapse(community_set_t * set) {
	community_t * communities = set->c;
	int new_comm_length = 0;
	char * full = (char *) calloc(set->num_communities, sizeof(char));
	//finds communities which have nodes in them
	for(int i = 0; i < set->num_communities; ++i) {
		if(communities[i].num_nodes) {
			full[i] = 1;
			++new_comm_length;
		}
		else
			full[i] = 0;
	}
	//creates a new pointer to the communities of the new size
	community_t * new_c = (community_t *) calloc(new_comm_length, sizeof(community_t));
	int i = 0;
	for(int j = 0; j < set->num_communities; ++j) {
		if(full[j]) {
			new_c[i] = communities[j];
			++i;
		}
	}

	//frees the space allocated for the array of communities
	free(set->c);
	set->c = new_c;
	//updates the set of communities
	set->num_communities = new_comm_length;

	free(full);

}

/**
 * Function to free the dynamically allocated memory for the set of communities.
 * @param set The set of communities to be freed.
 */
void clean_set(community_set_t * set) {
	//clears all nodes in the communities
	community_t * comm = set->c;
	for(int i = 0; i < set->num_communities; ++i) {
		clear((comm+i)->nodes);
	}
	//frees the space allocated for the array of communities
	free(comm);
	//frees the space allocated for the set
	free(set);
}

/**
 * Function to print the communities.
 * @param set The set one wants to print the communities of.
 */
void print_communities(community_set_t * set) {
	printf("ci sono %d comunita'\n\n", set->num_communities);
	for(int i = 0; i < set->num_communities; ++i)
		printf("Comunita' %d ha %d nodi, peso interno %d, peso esterno %d\n", i, set->c[i].num_nodes,set->c[i].internal_weight,set->c[i].external_weight);
	puts("");
}
