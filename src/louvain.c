/*
 ============================================================================
 Name        : louvain.c
 Author      : Buratto Alessandro
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "graph.h"
#include "community.h"

#define MAX_LINE_LENGTH 40
#define PRECISION 0.000001
#define DEF_NUM_PASS -1
#define MAX_LINES 1000000

/**
 * @def MAX_LINES
 * The maximum number of lines that the input file can have
 * @def DEF_NUM_PASS
 * The default max number of repetitions of phase 1 before phase 2 of the algorithm
 * @def MAX_LINE_LENGTH
 * The maximum length of the input string read in the input file.
 * @def PRECISION
 * The minimum improvement in the modularity needed to continue with the iterations
 */

/**
 * Function to create a graph from a set of communities, it also frees the memory allocated for the given set and the old graph.
 * @param set The set of communities to translate
 * @return A pointer to the new graph created
 */
graph_t * comm2graph(community_set_t * set) {
	graph_t * out = NULL;
	community_t * communities = set->c;
	//allocates memory for the array of edges
	edge_t * edges = (edge_t *) calloc(pow(set->num_communities, 2), sizeof(edge_t));
	int size = 0;
	//cycles through all communities
	for(int i = 0; i < set->num_communities; ++i) {
		//internal weight is a self loop on the node
		if(communities[i].internal_weight) {
			edge_t e = {i, i, communities[i].internal_weight};
			edges[size++] = e;
		}
		int * edge_comm_w = (int *) calloc(set->num_communities, sizeof(int));
		node_t * list = communities[i].nodes;
		//cycle through all the nodes in the community to find edges to other communities
		while(list) {
			//list of edges of the node in the community
			gnode_t * head = set->g->vertices[list->val];
			//calculates the weight for each link in the new graph
			while(head) {
				int comm = find_community(set, head->dest);
				edge_comm_w[comm] += head->weight;
				head = head->next;
			}
			list = list->next;
		}
		//creates the edges and adds them to the array
		//don't care to add links to communities before the current one
		for(int j = i+1; j < set->num_communities; ++j) {
			//the link has weight > 1
			if(edge_comm_w[j]) {
				edge_t e = {i, j, edge_comm_w[j]};
				edges[size++] = e;
			}
		}
		free(edge_comm_w);
	}
	//create the new graph
	out = create_graph(edges, size);
	free(edges);
	//frees the current community and graph
	clean_graph(set->g);
	clean_set(set);

	return out;
}

/**
 * Function that implements the Louvain algorithm for community detection, it is divided into two phases that are repeated iteratively until there can't be an improvement in modularity.
 * @param g The given initial graph to start the modularity optimization.
 * @param n The number of passes to be done in phase 1 before moving to phase 2.
 * @return A pointer to a graph containing the optimized network. Communities are now represented by nodes and arches between them represent connections between communities.
 */
graph_t * louvain(graph_t * g, int n) {
	graph_t * graph = g;
	int const num_pass = n;
	int k = 0;//iteration count
	double new_modularity, mod;
	do{
		++k;
		printf("Iterazione numero %d\n", k);
		//create a community for each node in the graph
		community_set_t * set = create_community_set(graph);
		char improvement;//flag to stop
		new_modularity = modularity(set);

		int num_pass_done = 0;

		do {
			improvement = 0;
			mod = new_modularity;
			++num_pass_done;

			//start iterations for each node in the graph
			for(int i = 0; i < graph->num_vertices; ++i) {
				int comm_index = find_community(set, i);
				community_t * communities = set->c;//pointer to the array of communities
				int best_comm = comm_index;//the current best community is the one already containing the current node
				double best_increment = 0;
				char * neigh_comm = neighboring_commuities(set, i);//array of all the connected communities
				remove_node_from_community(set,comm_index,i);//remove the current node from it's own community
				//foreach community calculate the change in modularity
				for(int j = 0; j < set->num_communities; ++j) {
					//not a neighboring community ==> skip iteration
					if(neigh_comm[j] == 0)
						continue;
					//else
					community_t * curr = &(communities[j]);//pointer to current community
					double m = (double) graph->tot_link_weight;//total weight of links in the graph
					double first_term = (((double) curr->internal_weight) + 2.0 * possible_new_links(set, j, i))/(2*m) - pow((((double) curr->external_weight+curr->internal_weight) + degree(graph, i))/(2*m), 2);
					double second_term = ((double) curr->internal_weight)/(2*m) - pow(((double) curr->external_weight+curr->internal_weight) / (2*m),2) - pow(((double) degree(graph, i)) / (2*m),2);
					double mod_change = first_term - second_term;

					if(mod_change > best_increment) {
						best_increment = mod_change;
						best_comm = j;
					}
				}


				add_node_to_community(set, best_comm, i);//add the node to the best community
				free(neigh_comm);//frees allocated memory
				//best community is different from starting community
				if(best_comm != comm_index)
					improvement = 1;
			}

			new_modularity = modularity(set);
			collapse(set);
					printf("Dopo la passata %d dell'iterazione %d ", num_pass_done, k);
					print_communities(set);
		}
		while(improvement && new_modularity - mod > PRECISION && num_pass_done != num_pass);


		//phase 2 of the algorithm
		graph = comm2graph(set);
		puts("Il nuovo grafo e':");
		print_graph(graph);
	}
	while(new_modularity - mod > PRECISION);

	return graph;
}

/**
 * Function to create a graph from an input file containing the nodes of the community stored as pairs source-destination
 * @param name The input file's name.
 * @return A pointer to the created graph.
 */
graph_t * read_input(const char * name) {
	int src, dest;
	edge_t * edges = (edge_t *) calloc(MAX_LINES, sizeof(edge_t));
	int size = 0;

	//opens the file in read mode
	FILE * finput = fopen(name, "r");
	char * input_buffer = (char *) malloc(MAX_LINE_LENGTH * sizeof(char));
	if(finput == 0) {
		puts("Il file non puo' essere aperto");
		exit(0);
	}
	while(fgets(input_buffer, MAX_LINE_LENGTH, finput)) {
		sscanf(input_buffer, "%d %d", &src, &dest);
		edge_t e = {src, dest, 1};
		edges[size++] = e;
	}

	graph_t * graph = create_graph(edges, size);
	//frees allocated memory
	free(edges);
	free(input_buffer);
	fclose(finput);
	return graph;
}

int main(int argc, char * argv[]) {
	graph_t * g = NULL;
	graph_t * g1 = NULL;
	//input file and no num pass
	if(argc == 2 || (argc == 4 && strcmp(argv[2], ">") == 0)) {
		g = read_input(argv[1]);
		puts("Il grafo iniziale e':");
		print_graph(g);
		g1 = louvain(g, DEF_NUM_PASS);
	}
	//input file + output redirection
	else if (argc == 4 || argc == 6) {
		g = read_input(argv[1]);
		if(strcmp(argv[2], "-p") == 0) {
			int const num_pass = atoi(argv[3]);
			puts("Il grafo iniziale e':");
			print_graph(g);
			g1 = louvain(g, num_pass);
		}
		else {
			puts("Option chosen is non existent");
			puts("Usage: ./louvain <input_filename> [-p <number_of_passes>]");
			exit(1);
		}
	}
	else {
		puts("Usage: ./louvain <input_filename> [-p <number_of_passes>]");
	}

	clean_graph(g1);

	return EXIT_SUCCESS;
}

