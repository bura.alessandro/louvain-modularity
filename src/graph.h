/*
 ============================================================================
 Name        : graph.h
 Author      : Buratto Alessandro
 ============================================================================
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#define MAX_NODES 100000


/**
 * @def MAX_NODES
 * The maximum number of nodes that the graph can contain.
 * @struct gnode_t
 * Structure that contains a node of the adjacency list of the links to a node of the graph.
 * @struct graph_t
 * Structure that contains a graph data structure by defining the adjacency list for all of its nodes. It also includes the total number of vertices and edges and the sum of all the edges in the network.
 * @struct edge_t
 * Structure that contains a pair source-destination and a weight to represent the link between these nodes.
 */
typedef struct gnode_t {int dest; struct gnode_t * next; int weight;} gnode_t;
typedef struct graph_t {gnode_t ** vertices; int num_vertices, num_edges, tot_link_weight;} graph_t;
typedef struct edge_t {int src, dest, weight;} edge_t;

graph_t * create_graph(edge_t * edges, int n);

int num_vertices(graph_t * g);

int num_edges(graph_t * g);

void print_graph(graph_t *);

int degree(graph_t * g, int node_id);

void add_edge(graph_t * g, edge_t e);

void remove_edge(graph_t * g, edge_t e);

void clean_graph(graph_t * g);

#endif /* GRAPH_H_ */
