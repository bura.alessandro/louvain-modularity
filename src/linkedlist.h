/*
 ============================================================================
 Name        : linkedlist.h
 Author      : Buratto Alessandro
 ============================================================================
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

/**
 * @struct node_t
 * Structure to contain a node in a linked list containing its value and the pointer to the next one.
 */
typedef struct node_t {int val; struct node_t * next;} node_t;

int pop(node_t ** head);

void push(node_t ** head, int val);

int remove_index(node_t ** head, int id);

void clear(node_t * head);


#endif /* LINKEDLIST_H_ */
